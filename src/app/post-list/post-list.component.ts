import {Component, Input, OnInit} from '@angular/core';
import {PostEntity} from "../entities/PostEntity";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input()
  private listeDePosts: PostEntity[];
  constructor() { }

  ngOnInit() {
  }

}
