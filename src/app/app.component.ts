import {Component} from '@angular/core';
import {PostEntity} from "./entities/PostEntity";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private postOne: PostEntity = new PostEntity('Mon Premier post', 'tkherbi9a zerbi9a', 0);
  private postTwo: PostEntity = new PostEntity('Mon deuxième post', 'tkherbi9a zerbi9a we7da khra', 0);
  private postThree: PostEntity = new PostEntity('Encore un post', 'tkherbi9a zerbi9a we7da khra twiiila kter', 0);

  private tableauDePosts: PostEntity[] = [];


  constructor() {
    this.tableauDePosts.push(this.postOne, this.postTwo, this.postThree);
    console.log(this.tableauDePosts);
  }
}
