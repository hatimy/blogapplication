import {Component, Input, OnInit} from '@angular/core';
import {PostEntity} from "../entities/PostEntity";

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input()
  private postItem: PostEntity;

  constructor() {
  }

  ngOnInit() {
    console.log(this.postItem);
  }

  onLikeIt() {
    this.postItem.loveIts++;
    console.log(this.postItem.loveIts);
  }

  onDislikeIt() {
    this.postItem.loveIts--;
    console.log(this.postItem.loveIts);
  }
}
